#pragma once

#ifndef GS_SBody
#define GS_SBody

#include <QColor>
#include <QtGlobal>
#include <SObject.hpp>

class SBody : public SObject {
  private:
	SObject *parent = nullptr;
	// Required
	qint16 a;     // Large semi - axis
	qreal e;      // eccentricity
	qreal i;      // inclination
	qreal lan;    // longitude of the ascending node
	qreal pa;     // pericenter argument
	qreal aa;     // average anomaly
	qint16 m;     // mas in earth mas
	qint16 r;     // radius in earth radius
	QColor color; // castom color

	// qint16 v; // orbital speed
	// qint16 density;

  public:
	SBody(SObject *const parent, const QString name, const qint16 &a,
		  const qreal &e, const qreal &i, const qreal &lan, const qreal &pa,
		  const qreal &aa, const qint16 &m, const qint16 &r,
		  const QColor &color);

	SBody(const QString name, const qint16 &a, const qreal &e, const qreal &i,
		  const qreal &lan, const qreal &pa, const qreal &aa, const qint16 &m,
		  const qint16 &r, const QColor &color);
	~SBody();
};

#endif
