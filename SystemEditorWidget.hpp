#pragma once

#ifndef GS_SystemEditorWidget
#define GS_SystemEditorWidget

#include <QWidget>

namespace Ui {
	class SystemEditorWidget;
}

class SystemEditorWidget : public QWidget {
	Q_OBJECT

  public:
	explicit SystemEditorWidget(QWidget *parent = nullptr);
	~SystemEditorWidget();

  signals:
	void backToMainSignal();

  private slots:
	void on_backAction_triggered();

  private:
	Ui::SystemEditorWidget *ui;
};

#endif
