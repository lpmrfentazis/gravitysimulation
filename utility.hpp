#pragma once

#ifndef GS_utility
#define GS_utility

#include <QColor>
#include <QtMath>

namespace utilites {

	// Сredit from https://habr.com/ru/post/427333/
	// http://www.vendian.org/mncharity/dir3/blackbody/UnstableURLs/bbr_color.html
	// It works correctly in the range from 1000 to 40000
	static QColor kelvitToRGB(qint16 kelvin) {
		qreal r, g, b;

		kelvin /= 100;

		// Calculate red part

		if (kelvin <= 66)
			r = 255;
		else {
			r = kelvin - 60;
			r = 329.698727446 * qPow(r, -0.1332047592);

			if (r < 0)
				r = 0;

			else if (r > 255)
				r = 255;
		}

		// Calculate green part

		if (kelvin <= 66) {
			g = kelvin;
			g = 99.4708025861 * qLn(g) - 161.1195681661;

			if (g < 0)
				g = 0;

			else if (g > 255)
				g = 255;

		} else {
			g = kelvin - 60;
			g = 288.1221695283 * qPow(g, -0.0755148492);

			if (g < 0)
				g = 0;
			else if (g > 255)
				g = 255;
		}

		// Calculate blue part

		if (kelvin >= 66)
			b = 255;
		else {

			if (kelvin <= 19)
				b = 0;
			else {
				b = kelvin - 10;
				b = 138.5177312231 * qLn(b) - 305.0447927307;

				if (b < 0)
					b = 0;
				else if (b > 255)
					b = 255;
			}
		}

		return QColor(r, g, b);
	}

} // namespace utilites
#endif
