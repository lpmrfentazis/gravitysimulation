#include "SStar.hpp"
#include "SObject.hpp"
#include "utility.hpp"
#include <QBrush>

SStar::SStar(const QString name, const qreal &r, const qreal &m,
			 const qint16 &t)
	: SObject(name) {
	this->name = name;
	this->r = r;
	this->m = m;
	this->t = t;

	this->setRect(0, 0, 40, 40);
	this->color = utilites::kelvitToRGB(t);
	// setBrush(QBrush(color));
	setBrush(color);
	// calculate color
}

SStar::SStar(SObject *const parent, const QString name, const qreal &r,
			 const qreal &m, const qint16 &t)
	: SObject(name) {
	this->parent = parent;
	this->name = name;

	this->r = r;
	this->m = m;
	this->t = t;

	this->setRect(0, 0, 40, 40);
	this->color = utilites::kelvitToRGB(t);
}

SStar::~SStar() {}
