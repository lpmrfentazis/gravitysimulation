#include "MainWindow.hpp"
#include "./ui_MainWindow.h"

#include "SceneWidget.hpp"
#include "SystemEditorWidget.hpp"

MainWindow::MainWindow(QWidget *parent)
	: QMainWindow(parent), ui(new Ui::MainWindow) {

	systemEditorWindow = new SystemEditorWidget();
	// System Editor Window Feedback
	connect(systemEditorWindow, &SystemEditorWidget::backToMainSignal, this,
			&MainWindow::show);

	sceneWindow = new SceneWidget();
	// Scene Window Feedback
	connect(sceneWindow, &SceneWidget::backToMainSignal, this,
			&MainWindow::show);
	ui->setupUi(this);
}

MainWindow::~MainWindow() {
	delete systemEditorWindow;
	delete ui;
}

void MainWindow::on_newButton_clicked() {
	systemEditorWindow->show();
	this->close();
}

void MainWindow::on_loadButton_clicked() {
	sceneWindow->show();
	this->close();
}
