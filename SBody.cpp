#include "SBody.hpp"
#include "SObject.hpp"
#include <QColor>

/*
	qint16 a;     // Large semi - axis
	qreal e;      // eccentricity
	qreal i;      // inclination
	qreal lan;    // longitude of the ascending node
	qreal pa;     // pericenter argument
	qreal aa;     // average anomaly
	qint16 m;     // mas in earth mas
	qint16 r;     // radius in earth radius
	QColor color; // castom color
*/

SBody::SBody(const QString name, const qint16 &a, const qreal &e,
			 const qreal &i, const qreal &lan, const qreal &pa, const qreal &aa,
			 const qint16 &m, const qint16 &r, const QColor &color)
	: SObject(name) {
	this->name = name;
}

SBody::SBody(SObject *const parent, const QString name, const qint16 &a,
			 const qreal &e, const qreal &i, const qreal &lan, const qreal &pa,
			 const qreal &aa, const qint16 &m, const qint16 &r,
			 const QColor &color)
	: SObject(name) {
	this->parent = parent;
	this->name = name;
}

SBody::~SBody() {}
