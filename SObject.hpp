#pragma once

#ifndef GS_SObject
#define GS_SObject

#include <QGraphicsEllipseItem>
#include <QString>

/**
 * @brief Abstract class describing a space object
 */
class SObject : public QGraphicsEllipseItem {
  public:
	QString name = "";

	SObject(const QString name);
	SObject(const SObject &other) = delete;
	SObject &operator=(SObject &othrer) = delete;
	~SObject();
};

#endif
