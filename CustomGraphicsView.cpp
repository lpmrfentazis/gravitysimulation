#include "CustomGraphicsView.hpp"
#include "SBody.hpp"
#include "SStar.hpp"
#include "utility.hpp"
#include <QGraphicsItemGroup>
#include <QGraphicsScene>
#include <QSizePolicy>
#include <QTimer>

#include <Settings.hpp>

CustomGraphicsView::CustomGraphicsView(QWidget *parent) {
	this->setAlignment(Qt::AlignCenter);
	this->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);
	this->setDragMode(QGraphicsView::ScrollHandDrag);

	this->setMinimumHeight(550);
	this->setMinimumWidth(780);

	sWidth = this->width();
	sHeight = this->height();

	scene = new QGraphicsScene();
	group = new QGraphicsItemGroup();

	this->setScene(scene);

	scene->addItem(group);

	timer = new QTimer();
	connect(timer, &QTimer::timeout, this, &CustomGraphicsView::timerAlarmSlot);

	// qint16 shift = sWidth / 2;

	SObject *sun = new SStar("Sun", 1, 1, 5780);
	// SObject *planet = new SBody(sun, "Planet");

	this->objects.push_back(sun);
	// this->objects.push_back(planet);
	//  this->objects.push_back(QGraphicsEllipseItem(0 + shift, 0 + shift,
	//  50, 50)); objects[0].setBrush(utilites::kelvitToRGB(5780));

	// frameTimeout from Settings.hpp
	for (auto item : objects)
		scene->addItem(item);

	timer->start(settings::frameTimeout);
}

void CustomGraphicsView::setSceneSize(qint16 x, qint16 y) {
	this->setSceneRect(0, 0, x, y);
}

CustomGraphicsView::~CustomGraphicsView() {
	delete scene;
	delete group;
	for (auto object : objects)
		delete object;
}

void CustomGraphicsView::timerAlarmSlot() {
	this->deleteItemsFromGroud(group);
	// scene->setSceneRect(0, 0, sWidth, sHeight);

	// auto circle = new QGraphicsEllipseItem(0 + shift, 0 + shift, 50, 50);

	// group->addToGroup(circle);

	// scene->addItem(group);
}

void CustomGraphicsView::flush() {
	foreach (auto *item, scene->items()) {
		delete item;
	};
}

// void CastomGraphicsView::resizeEvent(QResizeEvent *event) {}

void CustomGraphicsView::deleteItemsFromGroud(QGraphicsItemGroup *group) {
	foreach (auto *item, scene->items(group->boundingRect())) {
		if (item->group() == group)
			delete item;
	};
};

void CustomGraphicsView::wheelEvent(QWheelEvent *event) {
	if (event->modifiers() & Qt::ControlModifier) {
		QGraphicsView::wheelEvent(event);
	} else {
		const ViewportAnchor anchor = transformationAnchor();
		setTransformationAnchor(QGraphicsView::AnchorUnderMouse);
		int angle = event->angleDelta().y();
		qreal factor;

		if (angle > 0) {
			factor = settings::scaleRate;
		} else {
			factor = 1 / settings::scaleRate;
		}

		scale(factor, factor);
		setTransformationAnchor(anchor);
	}
}
