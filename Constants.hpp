#ifndef GS_Constants
#define GS_Constants
#pragma once

#include <QtGlobal>

namespace constants {
	// All the masses are divided on 10^24 for fit into types
	constexpr qreal earthMas = 5.9742;
	constexpr qreal sunMas = 1.98892 * 10e6; // 1.98892 * 10^6

	constexpr qreal earthRadius = 6378 * 10e3;
	constexpr qreal sunRadius = 696340 * 10e3;

	constexpr qreal sunTemperature = 5780;
	// constexpr qreal sunLuminosity = 696340 * 10e3;

} // namespace constants

#endif
