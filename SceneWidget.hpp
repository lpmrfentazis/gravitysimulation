#pragma once

#ifndef GS_SceneWidget
#define GS_SceneWidget

#include "CustomGraphicsView.hpp"
#include <QWidget>

namespace Ui {
	class SceneWidget;
}

class SceneWidget : public QWidget {
	Q_OBJECT

  public:
	explicit SceneWidget(QWidget *parent = nullptr);
	~SceneWidget();

  signals:
	void backToMainSignal();

  private slots:
	void on_backAction_triggered();

  private:
	CustomGraphicsView *view;
	Ui::SceneWidget *ui;
	// void resizeEvent(QResizeEvent *event);
};

#endif
