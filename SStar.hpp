#pragma once

#ifndef GS_SStar
#define GS_SStar

#include <QColor>
#include <QtGlobal>
#include <SObject.hpp>

class SStar : public SObject {
  private:
	SObject *parent = nullptr;
	// Required
	qreal r;  // radius in sun radius
	qreal m;  // mas in sun mas
	qint16 t; // temperature in sun temperature
	// qreal l;  // luminosity in sun luminosity

	// Calculated after init
	// QString spectralClass;
	// qreal density;
	QColor color;

  public:
	SStar(SObject *const parent, const QString name, const qreal &r,
		  const qreal &m, const qint16 &t);
	SStar(const QString name, const qreal &r, const qreal &m, const qint16 &t);
	~SStar();
};

#endif
