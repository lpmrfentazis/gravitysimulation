#pragma once

#ifndef GS_MainWindow
#define GS_MainWindow

#include "SceneWidget.hpp"
#include "SystemEditorWidget.hpp"
#include <QtWidgets/QMainWindow>

QT_BEGIN_NAMESPACE
namespace Ui {
	class MainWindow;
}
QT_END_NAMESPACE

class MainWindow : public QMainWindow {
	Q_OBJECT

  public:
	MainWindow(QWidget *parent = nullptr);
	~MainWindow();

  private slots:
	void on_newButton_clicked();

	void on_loadButton_clicked();

  private:
	SystemEditorWidget *systemEditorWindow;
	SceneWidget *sceneWindow;
	Ui::MainWindow *ui;
};

#endif
