#include "SystemEditorWidget.hpp"
#include "ui_SystemEditorWidget.h"

SystemEditorWidget::SystemEditorWidget(QWidget *parent)
	: QWidget(parent), ui(new Ui::SystemEditorWidget) {

	ui->setupUi(this);
}

SystemEditorWidget::~SystemEditorWidget() { delete ui; }

void SystemEditorWidget::on_backAction_triggered() {
	this->close();
	emit backToMainSignal();
}
