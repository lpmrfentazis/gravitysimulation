#ifndef GS_Settings
#define GS_Settings
#pragma once

#include <QtGlobal>
namespace settings {

	// inline excludes unintentional redefinition
	inline qint16 FPS = 60;
	inline qint16 frameTimeout = 1000 / FPS;
	inline qreal scaleRate = 0.95; // 5 percent

} // namespace settings
#endif
