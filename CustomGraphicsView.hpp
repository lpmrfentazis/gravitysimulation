﻿#pragma once

#ifndef GS_CastomGraphicsView
#define GS_CastomGraphicsView

#include "SObject.hpp"
#include <QGraphicsItem>
#include <QGraphicsItemGroup>
#include <QGraphicsView>
#include <QResizeEvent>
#include <QTimer>
#include <QVector>
#include <QWidget>
#include <QtGlobal>
#include <vector>

/**
 * @brief Castom class for creating space system scene
 */
class CustomGraphicsView : public QGraphicsView {
	Q_OBJECT
  public:
	explicit CustomGraphicsView(QWidget *parent = nullptr);
	void setSceneSize(qint16 x, qint16 y);
	~CustomGraphicsView();

  signals:

  private slots:
	// The timer helps set the frame rate
	void timerAlarmSlot();

  private:
	// Scene sizes
	qint16 sWidth;
	qint16 sHeight;

	QGraphicsScene *scene;
	QGraphicsItemGroup *group;
	QVector<SObject *> objects;

	QTimer *timer;

	// Castom resize event
	// void resizeEvent(QResizeEvent *event);

	void wheelEvent(QWheelEvent *event);

	// Might not be used
	void deleteItemsFromGroud(QGraphicsItemGroup *group);

	void flush();
};

#endif
