#include "SceneWidget.hpp"
#include "CustomGraphicsView.hpp"
#include "Settings.hpp"
#include "ui_SceneWidget.h"

SceneWidget::SceneWidget(QWidget *parent)
	: QWidget(parent), ui(new Ui::SceneWidget) {
	ui->setupUi(this);

	view = new CustomGraphicsView();
	ui->verticalLayout->addWidget(view);

	view->setSceneSize(this->width() * 2, this->width() * 2);
}

void SceneWidget::on_backAction_triggered() {
	this->close();
	emit backToMainSignal();
}

SceneWidget::~SceneWidget() {
	delete ui;
	delete view;
}

/*
void SceneWidget::resizeEvent(QResizeEvent *event) {
	view->setSceneSize(this->width() * 2, this->width() * 2);
}
*/
